# rasterized_border


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

The dataset is based on the data from eurostat, of the number of people working in a different country per NUTS2 [1].

Then, the assumption is made that the trans border workers are living within 60km from the border, and thus, the ratio is calculated taking into account the population in this area only. 

### Limitations of the dataset
The assumption of the 60km area could be discussed. 


### References
[1] [Eurostat](https://ec.europa.eu/eurostat/databrowser/view/LFST_R_LFE2ECOMM__custom_4016548/default/table?lang=en)


## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.

